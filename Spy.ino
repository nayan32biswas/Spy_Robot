#include <AFMotor.h>
#include <Servo.h>              // Add library

Servo servo;

int servo_position = 0 ;

AF_DCMotor motorFR(1, MOTOR12_64KHZ);
AF_DCMotor motorFL(2, MOTOR12_64KHZ);
AF_DCMotor motorBR(3, MOTOR34_64KHZ);
AF_DCMotor motorBL(4, MOTOR34_64KHZ);

int command;            //Int to store app command state.
int speedCar = 100;     // 50 - 255.
int speed_Coeff = 4;

void setup() {
  Serial.begin(9600);
  servo.attach (10);
  motorFR.setSpeed(0);
  motorFL.setSpeed(0);
  motorBR.setSpeed(0);
  motorBL.setSpeed(0);

}

void set_speed(int x) {
  motorFR.setSpeed(x);
  motorFL.setSpeed(x);
  motorBR.setSpeed(x);
  motorBL.setSpeed(x);
}

void goAhead() {
  motorFR.run(FORWARD);
  motorFL.run(FORWARD);
  motorBR.run(FORWARD);
  motorBL.run(FORWARD);
  set_speed(speedCar);
  Serial.println("goint to front");
}

void goBack() {
  motorFR.run(BACKWARD);
  motorFL.run(BACKWARD);
  motorBR.run(BACKWARD);
  motorBL.run(BACKWARD);
  set_speed(speedCar);
  Serial.println("goint to back");
}

void goRight() {
  motorFR.run(BACKWARD);
  motorFL.run(FORWARD);
  motorBR.run(BACKWARD);
  motorBL.run(FORWARD);
  set_speed(speedCar);
  Serial.println("going to right");
}

void goLeft() {
  motorFR.run(FORWARD);
  motorFL.run(BACKWARD);
  motorBR.run(FORWARD);
  motorBL.run(BACKWARD);
  set_speed(speedCar);
  Serial.println("going to left");
}

void servoRotateRight() {
  if (servo_position > 5) {
    servo_position = servo_position - 1;
    Serial.println(servo_position);
    servo.write(servo_position);
  }
}
void servoRotateLeft() {
  if (servo_position < 175) {
    servo_position = servo_position + 1;
    Serial.println(servo_position);
    servo.write(servo_position);
  }

}


void stopRobot() {
  motorFR.run(RELEASE);
  motorFL.run(RELEASE);
  motorBR.run(RELEASE);
  motorBL.run(RELEASE);
  Serial.println("stop");
}

void loop() {

  if (Serial.available() > 0) {
    command = Serial.read();
    Serial.println(command);
    stopRobot();      //Initialize with motors stopped.

    switch (command) {
      case 'F': goAhead(); break;
      case 'B': goBack(); break;
      case 'L': goLeft(); break;
      case 'R': goRight(); break;
      case 'H': servoRotateLeft(); delay(10); break; // I
      case 'J': servoRotateRight(); delay(10); break; // G
      //case 'J':goBackRight();break;
      //case 'H':goBackLeft();break;
      case '0': speedCar = 100; break;
      case '1': speedCar = 115; break;
      case '2': speedCar = 130; break;
      case '3': speedCar = 145; break;
      case '4': speedCar = 160; break;
      case '5': speedCar = 175; break;
      case '6': speedCar = 190; break;
      case '7': speedCar = 205; break;
      case '8': speedCar = 220; break;
      case '9': speedCar = 235; break;
      case 'q': speedCar = 255; break;
    }
  }
}

